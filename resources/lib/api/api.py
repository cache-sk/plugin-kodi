"""
    Stream Cinema backend API.
"""
import requests

from resources.lib.const import ENDPOINT, GENERAL, ORDER, SORT, QUERY, FILTER, HTTP_METHOD, STRINGS, AUTH, SETTINGS, \
    LANG
from resources.lib.gui import InfoDialog, InfoDialogType
from resources.lib.kodilogging import logger
from resources.lib.storage.settings import settings
from resources.lib.utils.kodiutils import get_string, datetime_to_str, get_timezone
from resources.lib.utils.url import Url
from resources.lib.vendor.lzstring import LZString
from resources.lib.wrappers.http import Http


class API(object):
    def __init__(self, plugin_version, uuid, api_url):
        self._api_url = api_url
        self._plugin_version = plugin_version
        self._uuid = uuid

    @staticmethod
    def _build_url(*args):
        return '/'.join(args)

    @staticmethod
    def get_source(item):
        source = item['_source']
        return source

    @staticmethod
    def get_hits(response):
        return response.get('hits')

    def request(self, method, url_path, body=None):
        headers = settings.common_headers()
        params = {}
        if 'access_token' not in url_path:
            params.update({'access_token': AUTH.TOKEN})
        return Http.request(
            method=method,
            json=body,
            url=self._api_url + url_path,
            headers=headers,
            params=params,
            timeout=GENERAL.API_TIMEOUT,
        )

    @staticmethod
    def api_response_handler(response):
        try:
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as e:
            InfoDialog(get_string(LANG.HTTP_ERROR), icon=InfoDialogType.ERROR).notify()
            logger.error(e)
            return None

    def media_played(self, media_id):
        return self.request(HTTP_METHOD.POST, self.URL.media_played(media_id))

    def media_detail(self, media_id):
        return self.request(HTTP_METHOD.GET, ENDPOINT.MEDIA_DETAIL.format(media_id=media_id))

    def report_stream(self, stream_id):
        return self.request(HTTP_METHOD.GET, ENDPOINT.STREAM_REPORT.format(stream_id=stream_id))

    def tv_range(self):
        return self.request(HTTP_METHOD.GET, ENDPOINT.TV_RANGE)

    def tv_date(self, date):
        return self.request(HTTP_METHOD.GET, ENDPOINT.TV_DATE.format(date=date))

    def get_all_pages(self, method, url, data=None):
        if data is None:
            data = {
                'totalCount': 0,
                'data': []
            }
        response = self.request(method, url).json()
        next_page = response.get('pagination', {}).get('next')
        data['data'] += response.get('data', [])
        if next_page:
            self.get_all_pages(method, next_page, data)
        return data

    class FILTER:
        @staticmethod
        def new_releases_dubbed(media_type, languages):
            return API.URL.media_filter(FILTER.NEWS_DUBBED, {
                QUERY.TYPE: media_type,
                QUERY.LANG: languages,
                QUERY.SORT: SORT.LANG_DATE_ADDED,
                QUERY.ORDER: ORDER.DESCENDING,
                QUERY.DAYS: settings[SETTINGS.NEWS_DUBBED_LIMIT],
            })

        @staticmethod
        def new_releases(media_type):
            return API.URL.media_filter(FILTER.NEWS, {
                QUERY.DAYS: settings[SETTINGS.NEWS_LIMIT],
                QUERY.TYPE: media_type,
                QUERY.SORT: SORT.DATE_ADDED,
                QUERY.ORDER: ORDER.DESCENDING
            })

        @staticmethod
        def ids(ids, limit=True):
            return API.URL.media_filter(FILTER.IDS, {
                QUERY.ID: ids,
            }, limit)

        @staticmethod
        def search(media_type, search_value):
            return API.URL.media_filter(FILTER.SEARCH, {
                QUERY.VALUE: search_value,
                QUERY.ORDER: ORDER.DESCENDING,
                QUERY.SORT: SORT.SCORE,
                QUERY.TYPE: media_type
            })

        @staticmethod
        def a_z(media_type, search_value):
            return API.URL.media_filter(FILTER.STARTS_WITH_SIMPLE, {
                QUERY.VALUE: search_value,
                QUERY.TYPE: media_type
            })

        @staticmethod
        def parent(parent_id):
            return API.URL.media_filter(FILTER.PARENT, {
                QUERY.VALUE: parent_id,
                QUERY.SORT: SORT.EPISODE
            })

        @staticmethod
        def service(media_type, service_name, service_ids, limit=True):
            return API.URL.media_filter(FILTER.SERVICE, {
                QUERY.SERVICE: service_name,
                QUERY.VALUE: service_ids,
                QUERY.TYPE: media_type
            }, limit)

        @staticmethod
        def sort(media_type, sort, order):
            return API.URL.media_filter(FILTER.SEARCH, {
                QUERY.ORDER: order,
                QUERY.SORT: sort,
                QUERY.TYPE: media_type
            })

        @staticmethod
        def count(media_type, count_type, value=STRINGS.EMPTY, from_item=0, limit=GENERAL.DIR_PAGE_LIMIT):
            return API.URL.media_filter_count(FILTER.ALL, count_type, {
                QUERY.TYPE: media_type,
                QUERY.VALUE: value,
                QUERY.FROM: from_item,
                QUERY.LIMIT: limit,
            })

        @staticmethod
        def most_watched(media_type):
            return API.URL.media_filter(FILTER.ALL, {
                QUERY.TYPE: media_type,
                QUERY.SORT: SORT.PLAY_COUNT,
                QUERY.ORDER: ORDER.DESCENDING
            })

        @staticmethod
        def popular(media_type):
            return API.URL.media_filter(FILTER.ALL, {
                QUERY.TYPE: media_type,
                QUERY.SORT: SORT.POPULARITY,
                QUERY.ORDER: ORDER.DESCENDING
            })

        @staticmethod
        def trending(media_type):
            return API.URL.media_filter(FILTER.ALL, {
                QUERY.TYPE: media_type,
                QUERY.SORT: SORT.TRENDING,
                QUERY.ORDER: ORDER.DESCENDING
            })

        @staticmethod
        def last_added(media_type):
            return API.URL.media_filter(FILTER.ALL, {
                QUERY.TYPE: media_type,
                QUERY.SORT: SORT.DATE_ADDED,
                QUERY.ORDER: ORDER.DESCENDING
            })

        @staticmethod
        def genre(media_type, search_value):
            return API.URL.media_filter(FILTER.GENRE, {
                QUERY.VALUE: search_value,
                QUERY.ORDER: ORDER.DESCENDING,
                QUERY.SORT: SORT.YEAR,
                QUERY.TYPE: media_type
            })

        @staticmethod
        def filter(media_type, filter_name, filter_value):
            return API.URL.media_filter(filter_name, {
                QUERY.VALUE: filter_value,
                QUERY.ORDER: ORDER.DESCENDING,
                QUERY.SORT: SORT.YEAR,
                QUERY.TYPE: media_type
            })

        @staticmethod
        def tv_program(media_type, date, station_name='', time=''):
            return API.URL.media_filter(date, {
                QUERY.TYPE: media_type,
                QUERY.STATION: station_name,
                QUERY.TIMEZONE: get_timezone(),
                QUERY.TIME: time,
            }, endpoint=ENDPOINT.TV_PROGRAM)

    class URL:
        @staticmethod
        def service(service_name, service_id):
            return ENDPOINT.SERVICE.format(service_name=service_name, service_id=service_id)

        @staticmethod
        def popular_media(media_type):
            return ENDPOINT.POPULAR.format(media_type=media_type)

        @staticmethod
        def media_played(media_id):
            return ENDPOINT.MEDIA_PLAYED.format(media_id=media_id)

        @staticmethod
        def media_detail(media_id):
            return ENDPOINT.MEDIA_DETAIL.format(media_id=media_id)

        @staticmethod
        def media_filter_count(filter_name, count_name, query):
            return ENDPOINT.FILTER_COUNT.format(filter_name=filter_name, count_name=count_name,
                                                query=Url.encode(query, True))

        @staticmethod
        def streams(media_id):
            return ENDPOINT.STREAMS.format(media_id=media_id)

        @staticmethod
        def media_filter(filter_name, query, limit=True, endpoint=ENDPOINT.FILTER):
            limit_size = settings[SETTINGS.PAGE_LIMIT] if limit else GENERAL.MAX_PAGE_LIMIT
            query.update({'limit': limit_size})
            query = Url.encode(query, True)
            if len(query) > GENERAL.MAX_URL_LENGTH:
                query = 'query=' + LZString.compressToEncodedURIComponent(query)
            return endpoint.format(filter_name, query=query)

        @staticmethod
        def tv_range():
            return ENDPOINT.TV_RANGE

        @staticmethod
        def tv_date(media_type, date):
            return ENDPOINT.TV_DATE.format(datetime_to_str(date, STRINGS.DATE), query=Url.encode({
                QUERY.TIMEZONE: get_timezone(),
                QUERY.TYPE: media_type
            }, True))
