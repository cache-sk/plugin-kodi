# -*- coding: utf-8 -*-
import datetime
import os
import time
import unicodedata

import xbmcvfs
import shutil

from resources.lib.api.api import API
from resources.lib.api.trakt_api import trakt
from resources.lib.compatibility import HTTPError
from resources.lib.const import SETTINGS, MEDIA_TYPE, URL, HTTP_METHOD, QUERY, FILTER, trakt_type_map, MEDIA_SERVICE, \
    TRAKT_LIST, PROTOCOL
from resources.lib.defaults import Defaults
from resources.lib.gui.renderers.dialog_renderer import DialogRenderer
from resources.lib.kodilogging import logger
from resources.lib.routing.router import Router
from resources.lib.storage.settings import settings
from resources.lib.storage.sqlite import DB
from resources.lib.utils.kodiutils import get_string, execute_json_rpc 


def normalize_name(name):
    name = name.replace(': ', ' ')
    name = name.replace(':', ' ')
    name = name.replace('*', '.')
    name = name.replace("'", '')
    name = unicodedata.normalize('NFKD', name)

    output = ''
    for c in name:
        if not unicodedata.combining(c):
            output += c

    return output


def write_nfo_file(name, media_detail):
    media_type = 'movie'
    if media_detail.get('info_labels').get('mediatype') == MEDIA_TYPE.TV_SHOW:
        media_type = 'tv'
    if 'services' in media_detail:
        services = media_detail['services']
        file = xbmcvfs.File(name, 'w')
        if 'csfd' in services:
            csfd_id = services['csfd']
            file.write(PROTOCOL.HTTPS + ':' + URL.CSFD_TITLE.format(csfd_id) + "\n")
        if 'tmdb' in services:
            tmdb_id = services['tmdb']
            file.write(PROTOCOL.HTTPS + ':' + URL.TMDB_TITLE.format(media_type, tmdb_id) + "\n")
        if 'imdb' in services:
            imdb_id = services['imdb']
            file.write(PROTOCOL.HTTPS + ':' + URL.IMDB_TITLE.format(imdb_id) + "\n")
        file.close()


def write_strm_file(path, content):
    file = xbmcvfs.File(path, 'w')
    file.write(content)
    file.close()


def delta_from_today(datestr):
    today = datetime.date.today()
    year = int(datestr[:4])
    month = int(datestr[5:7])
    day = int(datestr[8:10])
    dateadded = datetime.date(year, month, day)
    delta = (today - dateadded).days
    return delta


def extract_ids(items, type_=None):
    ids = []
    for i in items:
        if type_:
            itype = type_
        else:
            itype = i['type']
        id = "{0}:{1}".format(trakt_type_map.get(itype), i[itype]['ids']['trakt'])
        if not id in ids:
            ids.append(id)

    return ids


def add_movie(mf, id, mediaDetail):
    info_labels = mediaDetail.get('info_labels')
    originalTitle = info_labels.get('originaltitle')
    year = info_labels.get('year')
    if (not originalTitle):
        i18n_info_labels = mediaDetail.get('i18n_info_labels')
        for i18n_info_label in i18n_info_labels:
            if i18n_info_label.get('title'):
                originalTitle = i18n_info_label.get('title')
                break

    originalTitle = normalize_name(originalTitle)
    dirName = mf + originalTitle + ' (' + str(year) + ')'
    if not fixed_xbmcvfs_exists(dirName):
        xbmcvfs.mkdir(dirName)
    strmFileName = os.path.join(dirName, originalTitle + ' (' + str(year) + ')' + '.strm')
    nfoFileName = os.path.join(dirName, originalTitle + ' (' + str(year) + ')' + '.nfo')

    write_nfo_file(nfoFileName, mediaDetail)
    moviePath = Router.get_stream_url(id, id)
    write_strm_file(strmFileName, moviePath)


def add_movie_one(mf, mediaid):
    logger.debug("Adding movie to library... BEGIN")

    api = Defaults.api()
    url = API.URL.media_detail(mediaid)
    detailContents = ''
    try:
        detailContents = api.request(HTTP_METHOD.GET, url)
    except HTTPError as err:
        logger.error(err)

    if (detailContents != ''):
        mediaDetail = detailContents.json()
        add_movie(mf, mediaid, mediaDetail)

        logger.debug("Adding movie to library... END")
        if settings[SETTINGS.LIBRARY_AUTO_UPDATE]: 
            execute_json_rpc({"jsonrpc": "2.0", "id": 1, "method": "VideoLibrary.Scan"})


def add_tvshow(tf, mediaid, notify=True):
    logger.debug("Adding TV show to library... BEGIN")
    subs = DB.TV_SHOWS_SUBSCRIPTIONS.get_all()
    logger.debug("Subscriptions: {0}".format(subs))

    api = Defaults.api()

    url = API.URL.media_detail(mediaid)
    detailContents = ''
    try:
        detailContents = api.request(HTTP_METHOD.GET, url)
    except HTTPError as err:
        logger.error(err)
    if (detailContents != ''):
        if (notify):
            dp = DialogRenderer.progress(get_string(30351), get_string(30352))
        mediaDetail = detailContents.json()
        info_labels = mediaDetail.get('info_labels')
        originalTitle = info_labels.get('originaltitle')
        year = info_labels.get('year')
        if (not originalTitle):
            i18n_info_labels = mediaDetail.get('i18n_info_labels')
            for i18n_info_label in i18n_info_labels:
                if i18n_info_label.get('title'):
                    originalTitle = i18n_info_label.get('title')
                    break

        originalTitle = normalize_name(originalTitle)
        dirName = tf + originalTitle + ' (' + str(year) + ')'
        if not fixed_xbmcvfs_exists(dirName):
            xbmcvfs.mkdir(dirName)
        strmFileName = os.path.join(dirName, originalTitle + ' (' + str(year) + ')' + '.strm')
        nfoFileName = os.path.join(dirName, originalTitle + ' (' + str(year) + ')' + '.nfo')

        write_nfo_file(nfoFileName, mediaDetail)

        url = API.URL.media_filter(FILTER.PARENT, {QUERY.VALUE: mediaid, QUERY.SORT: 'episode'}, False)
        tvShowContents = ''
        try:
            tvShowContents = api.request(HTTP_METHOD.GET, url)
        except HTTPError as err:
            logger.error("URL: {0}".format(url))
            logger.error(err)
        if (tvShowContents != ''):
            seasonData = tvShowContents.json()
            seasonsCount = seasonData['totalCount']
            if seasonsCount == 0:
                shutil.rmtree(dirName, ignore_errors=True)
                if notify:
                    dp.close()
                logger.debug('Adding TV show to library, cannot add empty TV show... END')
                return
            mediaid = seasonData['data'][0]['_source']['root_parent']

            s = 1;
            seasons = seasonData['data']
            for season in seasons:
                seasonNo = season['_source']['info_labels']['season']
                if seasonNo == 0:
                    seasonNo = 1
                episodeNo = season['_source']['info_labels']['episode']

                if (episodeNo != 0):
                    seasonDirName = os.path.join(dirName, "Season 01")
                    if not fixed_xbmcvfs_exists(seasonDirName):
                        xbmcvfs.mkdir(seasonDirName)
                    episodeId = season['_id']
                    parentId = season['_source']['root_parent']
                    episodeURL = Router.get_stream_url(episodeId, parentId)
                    episodeFileName = "S{0}E{1}.strm".format(str(seasonNo).zfill(2), str(episodeNo).zfill(2))
                    write_strm_file(os.path.join(seasonDirName, episodeFileName), str(episodeURL))

                else:
                    currentSeason = "Season {0}".format(str(seasonNo).zfill(2))
                    seasonId = season["_id"]
                    seasonDirName = os.path.join(dirName, currentSeason)
                    if not fixed_xbmcvfs_exists(seasonDirName):
                        xbmcvfs.mkdir(seasonDirName)

                    url = API.URL.media_filter(FILTER.PARENT, {QUERY.VALUE: seasonId, QUERY.SORT: 'episode'}, False)
                    episodesContents = ''
                    try:
                        episodesContents = api.request(HTTP_METHOD.GET, url)
                    except HTTPError as err:
                        logger.error(err)
                    if (episodesContents != ''):
                        episodesData = episodesContents.json()
                        episodes = episodesData['data']
                        episodesCount = episodesData['totalCount']
                        for episode in episodes:
                            seasonNo = episode['_source']['info_labels']['season']
                            if seasonNo == 0:
                                seasonNo = 1
                            episodeNo = episode['_source']['info_labels']['episode']
                            episodeFileName = "S{0}E{1}.strm".format(str(seasonNo).zfill(2), str(episodeNo).zfill(2))
                            episodeId = episode['_id']
                            episodeURL = Router.get_stream_url(episodeId, seasonId)
                            write_strm_file(os.path.join(seasonDirName, episodeFileName), str(episodeURL))
                s = s + 1
                if (notify):
                    dp.update(int(100 * float(s) / float(len(seasons))))

        DB.TV_SHOWS_SUBSCRIPTIONS.add(mediaid, dirName)
        subs = DB.TV_SHOWS_SUBSCRIPTIONS.get_all()
        logger.debug("Subscriptions: {0}".format(subs))

        if (notify):
            dp.close()
            if settings[SETTINGS.LIBRARY_AUTO_UPDATE]:
                execute_json_rpc({"jsonrpc": "2.0", "id": 1, "method": "VideoLibrary.Scan"})
        logger.debug("Adding TV show to library... END")


def update_news(mf):
    logger.debug("Checking new movies in 'News' and 'News dubbed'... BEGIN")

    days = settings[SETTINGS.LIBRARY_AUTO_COUNT]

    api = Defaults.api()
    run_scan = False

    # check for new movies
    if settings[SETTINGS.LIBRARY_AUTO_NEW]:
        url = API.FILTER.new_releases(MEDIA_TYPE.MOVIE)
        newsList = ''
        try:
            newsList = api.request(HTTP_METHOD.GET, url)
        except HTTPError as err:
            logger.error("URL: {0}".format(url))
            logger.error(err)

        if newsList != '':
            run_scan = True
            data = newsList.json().get('data')
            for movie in data:
                id = movie.get('_id')
                delta = delta_from_today(movie.get('_source').get('info_labels').get('dateadded')[:10])
                logger.debug('Movie id: {0}, delta: {1}'.format(id, delta))
                if delta <= days:
                    mediaDetail = movie.get('_source')
                    add_movie(mf, id, mediaDetail)
                else:
                    break

    # check for new movies dubbed in preferred languages
    if settings[SETTINGS.LIBRARY_AUTO_NEW_DUBBED]:
        languages = settings.get_languages(SETTINGS.PREFERRED_LANGUAGE, SETTINGS.FALLBACK_LANGUAGE)
        url = API.FILTER.new_releases_dubbed(MEDIA_TYPE.MOVIE, languages)

        newsDubbedList = ''
        try:
            newsDubbedList = api.request(HTTP_METHOD.GET, url)
        except HTTPError as err:
            logger.error("URL: {0}".format(url))
            logger.error(err)

        if newsDubbedList != '':
            run_scan = True
            data = newsDubbedList.json().get('data')
            for movie in data:
                id = movie.get('_id')
                audio_languages = movie.get('_source').get('available_streams').get('audio_languages')
                delta = 999
                for audio_lang in audio_languages:
                    if audio_lang.get('lang') in languages:
                        delta_lang = delta_from_today(audio_lang.get('date_added')[:10])
                        if delta_lang < delta:
                            delta = delta_lang
                logger.debug('Movie id: {0}, delta: {1}'.format(id, delta))
                if delta <= days:
                    mediaDetail = movie.get('_source')
                    add_movie(mf, id, mediaDetail)
                else:
                    break

    logger.debug("Checking new movies in 'News' and 'News dubbed'... END")
    return run_scan


def fixed_xbmcvfs_exists(path):
    """
    Due to bug in kodi, xbmcvfs.exists have inconsistent behauviour checking dir / file on Win32 and using network share.
    This fix directorie's trail slash, because on scc library function there are no other files except .strm and .nfo.
    @return: True if directory / file exists
    """
    
    suffix = os.path.splitext(path)[1]
    if suffix not in ['.strm', '.nfo']:
        path = os.path.join(path, '')
    
    if xbmcvfs.exists(path):
        return True
    else:
        return False


def season_dir_check(directory):
    if (len(directory) == 9) and (directory[:7] == 'Season ') and (directory[-2:].isdigit()):
        return True
    else:
        return False


def update_tv_shows():
    api = Defaults.api()
    run_scan = False
 
    logger.debug("Checking new content for TV shows... BEGIN")

    shows_list = DB.TV_SHOWS_SUBSCRIPTIONS.get_all()
    logger.debug("Subscriptions: {0}".format(shows_list))
    if (len(shows_list)) == 0:
        logger.debug("Checking new content for TV shows - no subscriptions... END")
        return run_scan

    for item in shows_list:
        mediaid = item[0]
        path = item[1]
        if not fixed_xbmcvfs_exists(path):
            # this means TW show has been removed from library, no need subscription
            logger.debug("TV show removed from library, removing subscription {0}".format(path))
            DB.TV_SHOWS_SUBSCRIPTIONS.delete(mediaid)
        else:
            logger.debug("Checking new content for {0}".format(path))
            # get last season and past episode from users library
            seasons = xbmcvfs.listdir(path)
            lastSeasonStr = ''
            lastSeason = 0
            for item in seasons[0]:
                if fixed_xbmcvfs_exists(os.path.join(path, item)) and season_dir_check(item):
                    lastSeasonStr = item
                    lastSeason = int(item[-2:])

            lastEpisode = 0
            lastEpisodeStr = ''
            episodes = xbmcvfs.listdir(os.path.join(path, lastSeasonStr))
            for item in episodes[1]:
                if (fixed_xbmcvfs_exists(os.path.join(path, lastSeasonStr, item)) and (item.endswith('.strm'))):
                    lastEpisodeStr = item
                    lastEpisode = int(item[4:-5])

            logger.debug('Highest season: {0}, highest episode: {1}'.format(lastSeason, lastEpisode))

            # get episodes list by mediaid and add what is missing - only newer episodes (seasons)
            url = API.URL.media_filter(FILTER.PARENT, {QUERY.VALUE: mediaid, QUERY.SORT: 'episode'}, False)
            try:
                tvShowContents = api.request(HTTP_METHOD.GET, url)
                time.sleep(1)
            except HTTPError as err:
                logger.error("URL: {0}".format(url))
                logger.error(err)
            if (tvShowContents != ''):
                seasonData = tvShowContents.json()
                for season in seasonData['data']:
                    seasonNo = season['_source']['info_labels']['season']
                    episodeNo = season['_source']['info_labels']['episode']
                    seasonid = season['_id']
                    root_parent = season['_source']['root_parent']

                    # only one season TV show
                    if (episodeNo != 0):
                        episodeid = season['_id']
                        if (episodeNo > lastEpisode):
                            # missing episode, add it
                            strmPath = os.path.join(path, 'Season 01', 'S01E{0}.strm'.format(str(episodeNo).zfill(2)))
                            strmContent = Router.get_stream_url(episodeid, root_parent)
                            write_strm_file(strmPath, strmContent)
                            run_scan = True
                    else:
                        # check highest episode in user library
                        if (seasonNo == lastSeason):
                            url = API.URL.media_filter(FILTER.PARENT, {QUERY.VALUE: seasonid, QUERY.SORT: 'episode'}, False)
                            try:
                                seasonContents = api.request(HTTP_METHOD.GET, url)
                                time.sleep(1)
                            except HTTPError as err:
                                logger.error("URL: {0}".format(url))
                                logger.error(err)
                            if (seasonContents != ''):
                                episodesData = seasonContents.json()
                                for episode in episodesData['data']:
                                    episodeNo = episode['_source']['info_labels']['episode']
                                    episodeid = episode['_id']
                                    # missing episode, add it
                                    if (episodeNo > lastEpisode):
                                        strmPath = os.path.join(path, 'Season {0}'.format(str(seasonNo).zfill(2)),
                                                        'S{0}E{1}.strm'.format(str(seasonNo).zfill(2),
                                                                             str(episodeNo).zfill(2)))
                                        strmContent = Router.get_stream_url(episodeid, seasonid)
                                        write_strm_file(strmPath, strmContent)
                                        run_scan = True
                        if (seasonNo > lastSeason):
                            url = API.URL.media_filter(FILTER.PARENT, {QUERY.VALUE: seasonid, QUERY.SORT: 'episode'}, False)
                            try:
                                seasonContents = api.request(HTTP_METHOD.GET, url)
                                time.sleep(1)
                            except HTTPError as err:
                                logger.error("URL: {0}".format(url))
                                logger.error(err)
                            if (seasonContents != ''):
                                episodesData = seasonContents.json()
                                # missing season, create season directory and add episodes
                                seasonDirName = os.path.join(path, 'Season {0}'.format(str(seasonNo).zfill(2)))
                                if not fixed_xbmcvfs_exists(seasonDirName):
                                    xbmcvfs.mkdir(seasonDirName)
                                for episode in episodesData['data']:
                                    episodeNo = episode['_source']['info_labels']['episode']
                                    episodeid = episode['_id']
                                    strmPath = os.path.join(seasonDirName, 'S{0}E{1}.strm'.format(str(seasonNo).zfill(2),
                                                                                        str(episodeNo).zfill(2)))
                                    strmContent = Router.get_stream_url(episodeid, seasonid)
                                    write_strm_file(strmPath, strmContent)
                                    run_scan = True

    subs = DB.TV_SHOWS_SUBSCRIPTIONS.get_all()
    logger.debug("Subscriptions: {0}".format(subs))
    logger.debug("Checking new content for TV shows... END")
    return run_scan


def update_watchlist(mf, tf):
    logger.debug("Checking movies and TV shows in Trakt.tv watchlist... BEGIN")

    run_scan_str = ''
    if not trakt.is_authenticated():
        logger.debug("Checking movies and TV shows in Trakt.tv watchlist - user not authenticated... END")
        return run_scan_str

    api = Defaults.api()
    user = trakt.get_user()
    watchlist = trakt.list_items(user.username, TRAKT_LIST.WATCHLIST)

    ids = extract_ids(watchlist)
    res = None
    url = API.FILTER.service(MEDIA_TYPE.ALL, MEDIA_SERVICE.TRAKT_WITH_TYPE, ids, False)
    try:
        res = api.get_all_pages(HTTP_METHOD.GET, url)
    except HTTPError as err:
        logger.error("URL: {0}".format(url))
        logger.error(err)

    if res is not None:
        items = res.get('data')

        for item in items:
            media_id = item.get('_id')
            media_detail = item.get('_source')
            mediatype = media_detail.get('info_labels').get('mediatype')
            if mediatype == MEDIA_TYPE.MOVIE:
                if mf != '':
                    add_movie(mf, media_id, media_detail)
                    run_scan_str = 'M' if run_scan_str == '' else 'A'
                else:
                    logger.debug('Cannot add movie from Trakt.tv watchlist to library, library folder for movies is not set!')
            elif mediatype == MEDIA_TYPE.TV_SHOW:
                if tf != '':
                    add_tvshow(tf, media_id, False)
                    run_scan_str = 'T' if run_scan_str == '' else 'A'
                else:
                    logger.debug('Cannot add TV show from Trakt.tv watchlist to library, library folder for TV shows is not set!')
            else:
                logger.debug('Cannot add trakt watchlist item to library, only movie and TV show can be added!')

    logger.debug("Checking movies and TV shows in Trakt.tv watchlist... END")
    return run_scan_str


def add_to_library(mediaid, mediatype):
    logger.debug("Adding to library, media_id: {0}, media_type: {1}".format(mediaid, mediatype))
    if mediatype == MEDIA_TYPE.MOVIE:
        mf = settings[SETTINGS.MOVIE_LIBRARY_FOLDER]
        add_movie_one(mf, mediaid)

    if mediatype == MEDIA_TYPE.TV_SHOW:
        tf = settings[SETTINGS.TV_SHOW_LIBRARY_FOLDER]
        add_tvshow(tf, mediaid)


def update_library():
    mf = settings[SETTINGS.MOVIE_LIBRARY_FOLDER]
    tf = settings[SETTINGS.TV_SHOW_LIBRARY_FOLDER]
    if (mf == '' and tf == ''):
        logger.debug('Update library - nothing to update, user does not use library!')
        return

    logger.debug('Update library... BEGIN')

    run_scan_movies = False
    run_scan_tvshows = False
    run_scan_str = ''

    if (mf != ''):
        run_scan_movies = update_news(mf)

    if (tf != ''):
        run_scan_tvshows = update_tv_shows()

    if settings[SETTINGS.LIBRARY_AUTO_TRAKT_WATCHLIST]:
        run_scan_str = update_watchlist(mf, tf)

    if (run_scan_str == 'A'):
        run_scan_movies = True
        run_scan_tvshows = True
    if (run_scan_str == 'M'):
        run_scan_movies = True
    if (run_scan_str == 'T'):
        run_scan_tvshows = True

    logger.debug('Update library... END')

    if settings[SETTINGS.LIBRARY_AUTO_UPDATE]:
        if (run_scan_movies or run_scan_tvshows):
            execute_json_rpc({"jsonrpc": "2.0", "id": 1, "method": "VideoLibrary.Scan"})
