import json

from resources.lib.const import URL
from resources.lib.utils.url import Url
from resources.lib.wrappers.http import Http


def get_highest_quality(formats):
    return max(formats, key=lambda x: x.get('bitrate'))


def get_yt_video_url(url):
    url_data = Url.urlparse(url)
    query = Url.parse_qs(url_data.query)
    video_id = query["v"][0]
    r = Http.get(url=URL.YOUTUBE_VIDEO_INFO % video_id)
    stream_data = Url.parse_qs(r.content.decode('ascii'))
    player_response = json.loads(stream_data["player_response"][0])
    if "streamingData" in player_response:
        formats = player_response["streamingData"]["formats"]
        highest_quality = get_highest_quality([format for format in formats if format.get('audioQuality')])

        return highest_quality['url']

